package dao

import com.google.cloud.datastore.Key
import com.typesafe.config.ConfigFactory
import model.Element
import org.scalatest.BeforeAndAfterEach
import org.scalatestplus.play.PlaySpec
import org.specs2.mock.Mockito

class ElementDaoSpec extends PlaySpec with Mockito with BeforeAndAfterEach {

  var savedKeys: List[Key] = List()

  override def afterEach(): Unit = {
    val baseDao = new BaseDao(ConfigFactory.load())
    savedKeys.foreach { key =>
      baseDao.deleteFromKey(key)
    }
  }

  "ElementDao" should {
    val element = Element(10, "apple-test")
    val element2 = Element(5, "grape-test")
    val spyElementDao = spy(new ElementDao(ConfigFactory.load()))

    "save" in {
      val savedElement = spyElementDao.save(element)
      savedKeys = savedElement.get.getKey :: savedKeys
      savedElement.isDefined mustBe true
    }

    "get" in {
      val savedElement1 = spyElementDao.save(element)
      savedKeys = savedElement1.get.getKey :: savedKeys
      val savedElement2 = spyElementDao.save(element2)
      savedKeys = savedElement2.get.getKey :: savedKeys
      val found = spyElementDao.get(element.food)
      found.isDefined mustBe true
    }
  }

}
