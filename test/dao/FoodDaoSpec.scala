package dao

import com.google.cloud.datastore.Key
import com.typesafe.config.ConfigFactory
import model.{Element, Food}
import org.scalatest.BeforeAndAfterEach
import org.scalatestplus.play.PlaySpec
import org.specs2.mock.Mockito

class FoodDaoSpec extends PlaySpec with Mockito with BeforeAndAfterEach {

  var savedKeys: List[Key] = List()

  override def afterEach(): Unit = {
    val baseDao = new BaseDao(ConfigFactory.load())
    savedKeys.foreach { key =>
      baseDao.deleteFromKey(key)
    }
  }

  "FoodDao" should {
    val food = Food("apple-test", "fruit")
    val food2 = Food("grapes-test", "fruit")
    val spyFoodDao = spy(new FoodDao(ConfigFactory.load()))

    "save" in {
      val savedElement = spyFoodDao.save(food)
      savedKeys = savedElement.get.getKey :: savedKeys
      savedElement.isDefined mustBe true
    }

    "get" in {
      val savedElement1 = spyFoodDao.save(food)
      savedKeys = savedElement1.get.getKey :: savedKeys
      val savedElement2 = spyFoodDao.save(food2)
      savedKeys = savedElement2.get.getKey :: savedKeys
      val found = spyFoodDao.get(food.name)
      found.isDefined mustBe true
    }
  }

}
