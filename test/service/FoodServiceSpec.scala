package service

import com.google.cloud.datastore.Entity
import com.typesafe.config.ConfigFactory
import dao.{BaseDao, FoodDao}
import org.mockito.Matchers.{eq => eqTo, _}
import model.Food
import org.mockito.Mockito._
import org.scalatest.OneInstancePerTest
import org.scalatestplus.play.PlaySpec
import org.specs2.mock.Mockito
import play.api.libs.json.JsResult.Exception

class FoodServiceSpec extends PlaySpec with OneInstancePerTest with Mockito {

  "FoodService" should {

    val mockFoodDao = mock[FoodDao]
    val spyFoodService = spy(new FoodService(mockFoodDao))
    val baseDao = new BaseDao(ConfigFactory.load())
    val food1 = Food("apple-test", "fruit-test")
    val food2 = Food("grapes-test", "fruit-test")

    "addFood when motherCategory is food" in {
      when(mockFoodDao.save(any[Food])).thenReturn(Option(
        Entity.newBuilder(baseDao.getFoodKey("key")).build()))
      val saved = spyFoodService.addFood(Food("fruit-test", "food"))
      saved.isDefined mustBe true
    }

    "addFood when motherCategory is not food" in {
      when(mockFoodDao.save(any[Food])).thenReturn(Option(
        Entity.newBuilder(baseDao.getFoodKey("key")).build()))
      when(mockFoodDao.get(any[String])).thenReturn(Option(Food("fruit-test", "food")))
      val saved = spyFoodService.addFood(food1)
      saved.isDefined mustBe true
    }

    "return error adding food when motherCategory is not found" in {
      when(mockFoodDao.get(any[String])).thenReturn(Option.empty)
      assertThrows[Exception](spyFoodService.addFood(food1))
    }

    "getFoodByCategory" in {
      when(mockFoodDao.getByCategory(eqTo("fruit-test"))).thenReturn(List(food1, food2))
      when(mockFoodDao.getByCategory(eqTo("apple-test"))).thenReturn(List.empty)
      when(mockFoodDao.getByCategory(eqTo("grapes-test"))).thenReturn(List.empty)
      val foods = spyFoodService.getFoodByCategory("fruit-test")
      foods.toString().equals("{\"fruit-test\":[{\"apple-test\":[]},{\"grapes-test\":[]}]}") mustBe true
    }

  }

}
