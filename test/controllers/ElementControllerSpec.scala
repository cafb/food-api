package controllers

import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerTest
import play.api.test.Helpers._
import play.api.test.{FakeRequest, Injecting}

class ElementControllerSpec extends PlaySpec with GuiceOneAppPerTest with Injecting {

  "ElementController" should {

    "put" in {
      val result = route(app, FakeRequest(PUT, "/food/content/apple")).get
      await(result)
    }

    "get" in {
      val result = route(app, FakeRequest(GET, "/food/content/apple")).get
      await(result)
    }



  }

}