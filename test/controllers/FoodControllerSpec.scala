package controllers

import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerTest
import play.api.test.Helpers._
import play.api.test.{FakeRequest, Injecting}

class FoodControllerSpec extends PlaySpec with GuiceOneAppPerTest with Injecting {

  "FoodController" should {

    "add" in {
      val result = route(app, FakeRequest(POST, "/food/food/fruit ")).get
      await(result)
    }

    "getByMotherCategory" in {
      val result = route(app, FakeRequest(GET, "/food/food ")).get
      await(result)
    }

  }

}
