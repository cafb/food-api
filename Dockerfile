FROM ysihaoy/scala-play:2.12.2-2.6.0-sbt-0.13.15

# caching dependencies
COPY ["build.sbt", "/tmp/build/"]
COPY ["project/plugins.sbt", "project/build.properties", "/tmp/build/project/"]

# copy code
COPY . /root/app/
WORKDIR /root/app
RUN sbt stage

# copy files
COPY start_server.sh common/start_server.sh
COPY key.json common/key.json

EXPOSE 8080

# start logstash and the app server
ENTRYPOINT bash ./common/start_server.sh