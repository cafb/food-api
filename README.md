# FOOD API

A simple project to add and get food category and food element.
Each category can have subcategories or food elements.
Ex:
```
Food
	|-Fruit 
		|-Grape
		|-Oragne
		|-Apple
			|-Honey Crisp
			|-Granny Smith
		|-Banana
	|-Vegetables
		|-Bell pepper
		|-Mushroom
			|-Shitaki
```
Food is the main cathegory.

### Run project

To run the project, you must execute:
```
docker build .
```
```
docker run -p8080:8080 -e GOOGLE_CLOUD_PROJECT=food-api-205503 -e GOOGLE_APPLICATION_CREDENTIALS=./common/key.json [imageCode]
```
Note that the key is only used in this project to test. In prod env, the authentication and project id are provided by gcloud.

### Endpoints

This project has endpoints to add food category (POST /food/:motherCategory/:foodName),
get all subcategories by category (GET /food/:motherCategory), add a food element (PUT /food/content/:foodName )
and get food element (GET /food/content/:foodName).

The endpoint to add category has a filter and requires the header `X-Admin-Key`. This key can be any value.
The endpoint to add a food element requires a JSON with the element `quantity`

Ex 1: to create the category apple make a POST /food/food/fruit and then POST /food/fruit/apple
	to add an element in apple make a PUT /food/content/apple with json {"quantity": 1}
	
Ex 2: to get all the subcategories of fruit category make GET /food/fruit

Ex 3: to know how many apples was inserted make GET /food/content/apple
