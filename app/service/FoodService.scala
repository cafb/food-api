package service

import javax.inject.Inject

import com.google.cloud.datastore.Entity
import dao.FoodDao
import model.Food
import org.slf4j.LoggerFactory
import play.api.libs.json.{JsError, JsObject, Json}
import play.api.libs.json.JsResult.Exception

class FoodService @Inject()(foodDao: FoodDao) {

  private val log = LoggerFactory.getLogger(classOf[FoodService])

  def addFood(food: Food): Option[Entity] = {
    if (food.motherCategory.equals("food")) {
      foodDao.save(food)
    } else {
      if (foodDao.get(food.motherCategory).isEmpty) {
        log.error(s"category=${food.motherCategory} doesn't exist")
        throw Exception(JsError.apply("category doesn't exist"))
      }
      foodDao.save(food)
    }
  }

  def getFoodByCategory(category: String): JsObject = {
    if (category.isEmpty || foodDao.get(category).isEmpty) {
      Json.obj()
    } else {
      val foods: List[Food] = foodDao.getByCategory(category)
      val children = foods.map(element => getFoodByCategory(element.name))
      Json.obj(category -> children)
    }
  }

}
