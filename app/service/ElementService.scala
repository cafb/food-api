package service

import javax.inject.Inject

import com.google.cloud.datastore.Entity
import dao.{ElementDao, FoodDao}
import model.Element
import org.slf4j.LoggerFactory
import play.api.libs.json.JsError
import play.api.libs.json.JsResult.Exception

class ElementService @Inject()(elementDao: ElementDao, foodDao: FoodDao) {

  private val log = LoggerFactory.getLogger(classOf[ElementService])

  def addElement(element: Element): Option[Entity] = {
    log.info(s"adding element=$element")
    if (foodDao.get(element.food).isEmpty) {
      throw Exception(JsError("food not found"))
    }
    elementDao.save(element)
  }

  def getElement(food: String): Option[Element] = {
    log.info(s"getting element with foodName=$food")
    elementDao.get(food)
  }

}
