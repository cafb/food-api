package model

case class Element(quantity: Int, food: String)

object Element {

  import play.api.libs.functional.syntax._
  import play.api.libs.json._

  val QUANTITY = "quantity"
  val FOOD = "food"

  implicit val elementWrites: Writes[Element] = (
    (JsPath \ QUANTITY).write[Int] and
      (JsPath \ FOOD).write[String]
    )(unlift(Element.unapply))
}
