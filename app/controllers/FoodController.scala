package controllers

import javax.inject.Inject

import filter.AdminFilter
import model.Food
import org.slf4j.LoggerFactory
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, AnyContent, ControllerComponents, Request}
import service.FoodService

class FoodController @Inject()(cc: ControllerComponents, foodService: FoodService, adminFilter: AdminFilter) extends AbstractController(cc) {

  private val log = LoggerFactory.getLogger(classOf[FoodController])

  def add(motherCategory: String, name: String) = adminFilter { implicit request: Request[AnyContent] =>
    log.info(s"request to add a food with motherCategory=$motherCategory and name=$name")
    foodService.addFood(Food(name, motherCategory))
    Ok
  }

  def getByMotherCategory(motherCategory: String) = Action { implicit request: Request[AnyContent] =>
   log.info(s"request to get by mother category with motherCategory=$motherCategory")
    val list = foodService.getFoodByCategory(motherCategory)
    Ok(Json.obj("categories" -> list))
  }

}
