package controllers

import javax.inject.Inject

import model.Element
import model.Element._
import org.slf4j.LoggerFactory
import play.api.libs.json.{JsValue, Json}
import play.api.mvc.{AbstractController, AnyContent, ControllerComponents, Request}
import service.ElementService

class ElementController @Inject()(cc: ControllerComponents, elementService: ElementService) extends AbstractController(cc){

  private val log = LoggerFactory.getLogger(classOf[ElementController])

  def put(food: String) = Action { implicit request: Request[AnyContent] =>
    val body: Option[JsValue] = request.body.asJson
    log.info(s"request to add an element with foodName=$food body=$body")
    if (body.isEmpty) {
      BadRequest("Json with quantity is missing")
    } else {
      val quantity = (body.get \ "quantity").asOpt[Int]
      if (quantity.isEmpty) {
        UnprocessableEntity("quantity is not defined in json")
      } else {
        elementService.addElement(Element(quantity.get, food))
        Ok
      }
    }
  }

  def get(food: String) = Action { implicit request: Request[AnyContent] =>
    log.info(s"request to get element with foodName=$food")
    val element = elementService.getElement(food)
    if (element.isEmpty) {
      NotFound
    } else {
      Ok(Json.obj("element" -> element))
    }
  }
}
