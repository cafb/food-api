package dao

import com.google.cloud.datastore._
import com.typesafe.config.Config

import scala.collection.JavaConverters._

class BaseDao(config: Config) {

  protected var FOOD_NAMESPACE: String = config.getString("datastore.food.namespace")

  protected val ELEMENT: String = config.getString("datastore.element.kind")

  protected val FOOD_KIND: String = config.getString("datastore.food.kind")

  private var DATASTORE: Datastore = DatastoreOptions.getDefaultInstance.getService

  def getElementKey(food: String): Key = {
    datastore.newKeyFactory.setNamespace(FOOD_NAMESPACE).setKind(ELEMENT).newKey(food)
  }

  def getFoodKey(food: String): Key = {
    datastore.newKeyFactory.setNamespace(FOOD_NAMESPACE).setKind(FOOD_KIND).newKey(food)
  }

  private def datastore: Datastore = {
    if (DATASTORE == null) {
      DATASTORE = DatastoreOptions.getDefaultInstance.getService
    }
    DATASTORE
  }

  def runQuery(query: Query[Entity]): QueryResults[Entity] = {
    datastore.run(query, ReadOption.eventualConsistency())
  }

  def createTransaction: Transaction = datastore.newTransaction

  def commitTransaction(txn: Transaction): Unit = {
    txn.commit
  }

  protected def getFromKey(key: Key): Entity = {
    //todo fix this once datastore.get(key) is resolved
    val element = datastore.get(List(key).asJava).asScala
    if (element.hasNext) {
      element.next()
    } else {
      null
    }
  }

  def deleteFromKey(key: Key): Unit = {
    datastore.delete(key)
  }

  protected def getFromKeys(keys: List[Key]): Iterator[Entity] = {
    datastore.get(keys.asJava).asScala
  }

  protected def putEntity(entity: Entity): Entity = {
    datastore.put(entity)
  }
}
