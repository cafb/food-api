package dao

import javax.inject.Inject

import com.google.cloud.datastore.{Entity, LongValue, StringValue}
import com.typesafe.config.Config
import model.Element

class ElementDao @Inject()(config: Config) extends BaseDao(config) {

  val FOOD = "food"
  val QUANTITY = "category"

  def save(element: Element): Option[Entity] = {
    val key = getElementKey(element.food)
    val responseEntity = Entity.newBuilder(key)
      .set(FOOD, StringValue.newBuilder(element.food).build())
      .set(QUANTITY, LongValue.newBuilder(element.quantity).build())
      .build()
    Option(putEntity(responseEntity))
  }

  def get(food: String): Option[Element] = {
    val key = getElementKey(food)
    val entity = getFromKey(key)
    if (entity == null) {
      Option.empty
    } else {
      Option(Element(entity.getLong(QUANTITY).toInt, entity.getString(FOOD)))
    }
  }
}
