package dao

import javax.inject.Inject

import com.google.cloud.datastore.StructuredQuery.PropertyFilter
import com.google.cloud.datastore.{Entity, Query, QueryResults, StringValue}
import com.typesafe.config.Config
import model.Food

class FoodDao @Inject()(config: Config) extends BaseDao(config) {

  val NAME = "name"
  val MOTHER_CATEGORY = "mother_category"

  def save(food: Food): Option[Entity] = {
    val key = getFoodKey(food.name)
    val responseEntity = Entity.newBuilder(key)
      .set(NAME, StringValue.newBuilder(food.name).build())
      .set(MOTHER_CATEGORY, StringValue.newBuilder(food.motherCategory).build())
      .build()
    Option(putEntity(responseEntity))
  }

  def get(name: String): Option[Food] = {
    val key = getFoodKey(name)
    val entity: Entity = getFromKey(key)
    if (entity == null) {
     Option.empty
    } else{
      Option(Food(entity.getString(NAME), entity.getString(MOTHER_CATEGORY)))
    }
  }

  def getAll: List[Food] = {
    val query = Query.newEntityQueryBuilder().setNamespace(FOOD_NAMESPACE).setKind(FOOD_KIND).build
    val results: QueryResults[Entity] = runQuery(query)
    var result: List[Food] = List()
    while (results.hasNext) {
      val element = results.next()
      result = Food(element.getString(NAME), element.getString(MOTHER_CATEGORY)) :: result
    }
    result
  }

  def getByCategory(category: String): List[Food] = {
    val query = Query.newEntityQueryBuilder().setNamespace(FOOD_NAMESPACE).setKind(FOOD_KIND)
      .setFilter(PropertyFilter.eq(MOTHER_CATEGORY, category)).build
    val results: QueryResults[Entity] = runQuery(query)
    var result: List[Food] = List()
    while (results.hasNext) {
      val element = results.next()
      result = Food(element.getString(NAME), element.getString(MOTHER_CATEGORY)) :: result
    }
    result
  }

}