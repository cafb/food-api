package filter

import javax.inject.Inject

import org.slf4j.LoggerFactory
import play.api.libs.json.JsError
import play.api.libs.json.JsResult.Exception
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}

class AdminFilter @Inject()(parser: BodyParsers.Default)(implicit ec: ExecutionContext)
  extends ActionBuilderImpl(parser) {

  private val log = LoggerFactory.getLogger(classOf[AdminFilter])

  override def invokeBlock[A](request: Request[A], block: (Request[A]) => Future[Result]): Future[Result] = {
    val header = request.headers.get("X-Admin-Key")
    if (header.isEmpty || header.get.isEmpty) {
      log.error("admin header is missing")
      //todo imp this exception and create an exception handler
      throw Exception(JsError("Not authorized"))
    }
    block(request)
  }
}
